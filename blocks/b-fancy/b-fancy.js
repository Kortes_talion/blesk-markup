(function(undefined) {
    BEM.DOM.decl('b-fancy', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                bemThis.elem('phone').find('.b-form-control__input').inputmask("+7 (999)999-99-99");
            }
        }
    });
})();
