(function(undefined) {
    BEM.DOM.decl('b-map', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                var point = bemThis.params.point;

                ymaps.ready(function(){
                    var myMap;
                    myMap = new ymaps.Map("map", {
                        center: point,
                        zoom: 15,
                        behaviors: ["scrollZoom","drag"]
                    });

                    myPlacemark = new ymaps.Placemark(point, {
                        hintContent: 'Урал блеск'
                    });
                    myMap.geoObjects.add(myPlacemark);
                });
            }
        }
    });
})();
