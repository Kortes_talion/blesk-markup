({
    mustDeps: [
        {
            block: 'i-clearfix'
        },
        {
            block: 'b-container'
        },
        {
            block: 'i-inline'
        },
        {
            block:'b-link'
        },
        {
            block: 'b-icon'
        },
        {
            block: 'i-font',
            mods: {face: 'nova-bold'}
        },
        {
            block: 'i-vendor',
            elem: 'fancybox'
        }
    ]
})
