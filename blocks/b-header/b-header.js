(function(undefined) {
    BEM.DOM.decl('b-header', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                bemThis.elem('feedback').fancybox({
                    padding: 0,
                    closeBtn: false,
                    fitToView: false
                });
            }
        }
    });
})();
